---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![w:400px](./img/terraform_logo.png)


Langage de configuration

---

| Au programme pour ce début de chapitre (suite après l'exercice) |
|-----------------------------------------------------------------|
| Structure des fichiers de configuration                         |
| Types de blocks                                                 |
| Types de données                                                |
| Resources : meta-arguments                                      |
| Resources : ordre de création                                   |
| State                                                           |
| Git : fichier .gitignore                                        |
| Version constraint & lock file                                  |

---

### Structure des fichier de configuration [:book:](https://www.terraform.io/language) [:book:](https://www.terraform.io/language/syntax/configuration)

Hashicorp Configuration Language (HCL). Fichiers `.tf` ou `.tf.json`.

```terraform
<BLOCK TYPE> "<BLOCK LABEL>" "<BLOCK LABEL>" {
  # Block body
  <IDENTIFIER> = <EXPRESSION> # Argument
}

# Exemple
resource "aws_vpc" "main" {
  cidr_block = var.base_cidr_block
}
```

:memo: Diférence arguments et attributs : les attributs sont read-only (exemple : [provider AWS](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloud9_environment_ec2))

---

### Séparation de la config en plusieurs fichiers [:book:](https://www.terraform.io/language/files#directories-and-modules)

La configuration peut être divisée en plusieurs fichiers dans un même répertoire. Il n'y a pas de différences du point de vue de Terraform.

- :page_facing_up:`main.tf`
- :page_facing_up:`providers.tf`
- :page_facing_up:`variables.tf`
- :page_facing_up:`outputs.tf`
- ...
- :page_facing_up:`network.tf`
- :page_facing_up:`security_groups.tf`

---

### Types de blocks

- :black_medium_square: Terraform Settings
- :black_medium_square: Provider Configuration
- :black_medium_square: Resource
- :black_square_button: Data
- :black_medium_square: Variable
- :black_square_button: Locals
- :black_medium_square: Output
- :black_square_button: Module

---

### Types de blocks

#### Terraform Settings [:book:](https://www.terraform.io/language/settings)

```
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  required_version = ">= 0.14.9"
}
```

Principalement pour définir la version de Terraform et des providers. Aussi pour setup le backend et les fonctionnalités experimentales.

---

### Types de blocks

#### Provider Configuration block [:book:](https://www.terraform.io/language/providers)

```terraform
provider "aws" {
  profile = "default"
  region  = "us-west-2"
}
```

Ce block permet de définir la configuration du provider (region, authentification, roles, tags par défaut, ...).

:information_source: La liste des arguments acceptés est indiquée dans la documentation du provider. [:link:](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)

---

### Types de blocks

#### Resource block [:book:](https://www.terraform.io/language/resources)

```terraform
resource "aws_instance" "app_server" {
  ami           = "ami-830c94e3"
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}
```

:information_source: La liste des arguments acceptés est indiquée dans la documentation du provider. [:link:](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloud9_environment_ec2)

---

### Types de blocks

#### Data block [:book:](https://www.terraform.io/language/data-sources)

```terraform
data "aws_ami" "example" {
  most_recent = true

  owners = ["self"]
  tags = {
    Name   = "app-server"
    Tested = "true"
  }
}
```

Permet d'obtenir des informations sur des resources créées en dehors de Terraform. :information_source: Liste des arguments : voir provider. [:link:](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami)

---

### Types de blocks

#### Variable block [:book:](https://www.terraform.io/language/values/variables)

```terraform
variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleAppServerInstance"
}
```

Pour définir un input à la configuration. Les valeurs peuvent êtres passées interactivement, ou avec le flag `-var`, ou `-var-file`.

Autres arguments disponibles : `validation`, `sensitive`, `nullable`.

---

### Types de blocks

#### Locals block [:book:](https://www.terraform.io/language/values/locals)

```terraform
locals {
  service_name = "forum"
  owner        = "Community Team"
  pi           = 3.14159
  prefix       = "${var.app_name}_${var.env_name}_"
}
```

Variables locales, pour définir des constantes ou éviter de répéter une longue expression plusieurs fois.

---

### Types de blocks

#### Output block [:book:](https://www.terraform.io/language/values/outputs)

```terraform
output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.app_server.id
}
```

- pour afficher une valeur dans le terminal après l'apply
- pour retourner une valeur dans un module

---

### Types de blocks

#### Module block [:book:](https://www.terraform.io/language/modules/syntax)

```terraform
module "servers" {
  source = "./app-cluster"

  servers = 5
}
```

Pour faire appel à un module enfant. Un module peut être local (user defined), ou provenir d'une registry.

Un module est une config Terraform réutilisable qui se trouve dans un autre répertoire qu'on peut importer.

---

### Référence à une valeur d'un autre bloc [:book:](https://www.terraform.io/language/expressions/references)

| Type de block   | Exemple de référence     |
|-----------------|--------------------------|
| resource        | `exemple`                |
| data            | `data.exemple`           |
| variable        | `var.exemple`            |
| locals          | `local.exemple` (sans s) |
| module (output) | `module.exemple`         |

Le nom de la ressource dans cet exemple est `exemple`, quel que soit son type.


---

### Types de données [:book:](https://www.terraform.io/language/expressions/types)

#### Types primitifs

:abcd: `string` (unicode)
:1234: `number` (integer or float)
:ballot_box_with_check: `bool` (`true` or `false`)
- :memo: un bool peut être utilisé dans une condition ternaire : `condition ? true_val : false_val` [:book:](https://www.terraform.io/language/expressions/conditionals)

---

### Types de données [:book:](https://www.terraform.io/language/expressions/types)

#### Types complexes

:small_blue_diamond: `list/tuple/set` : `["a", 15, true]`
:small_orange_diamond: `map/object` : `{ name = "John", age = 32 }`

> In most situations, lists and tuples behave identically, as do maps and objects. [:book:](https://www.terraform.io/language/expressions/types#more-about-complex-types)

La différence est expliquée dans la documentation [:book: Type Constraints](https://www.terraform.io/language/expressions/type-constraints) (collection types VS structural types).

Les `set` ne sont pas ordonnés et ne contiennent que des valeurs uniques.

---

### Types de données [:book:](https://www.terraform.io/language/expressions/types)

#### Valeur null


La valeur `null` n'a pas de type.
Elle peut remplacer n'importe quel type et ne peut pas être utilisée pour les variables ayant l'argument `nullable = false`.

---

### Types de données [:book:](https://www.terraform.io/language/expressions/types)

#### Conversion automatique [:book:](https://www.terraform.io/language/expressions/types#type-conversion)

Terraform convertit automatiquement les types primitifs :

- `true` peut être converti en `"true"`, et vice-versa
- `false` peut être converti en `"false"`, et vice-versa
- `15` peut être converti en `"15"`, et vice-versa

Idem pour les types complexes inter-compatibles : `list/tuple/set`, et `map/object`.

---

### Ressources : meta-arguments (introduction)

Permet de modifier le comportement d'une ressource.
- `count` : pour créer plusieurs instances d'une même ressource [:book:](https://www.terraform.io/language/meta-arguments/count)
- `for_each` : comme count, mais en fonction d'une map ou d'un set de string [:book:](https://www.terraform.io/language/meta-arguments/for_each)
- `depends_on` : pour définir explicitement l'ordre de création des resources [:book:](https://www.terraform.io/language/meta-arguments/depends_on)
- `lifecycle` : pour modifier le comportement par défaut du cycle de vie des ressources [:book:](https://www.terraform.io/language/meta-arguments/lifecycle)
- `provider` : pour utiliser un alias de provider (exemple : multi-region) [:book:](https://www.terraform.io/language/meta-arguments/resource-provider)

---

### Ressources : ordre de création [:book:](https://www.terraform.io/language/resources/behavior#resource-dependencies)

L'ordre de création des ressources est déterminé implicitement à partir des références entre les ressources.

```terraform
resource "random_pet" "name" {}

resource "aws_instance" "web" {
  ami                    = "ami-a0cfeed8"
  instance_type          = "t2.micro"
  user_data              = file("init-script.sh")

  tags = {
    Name = random_pet.name.id
  }
}
```

---

### State [:book:](https://www.terraform.io/language/state)

Le fichier de state `terraform.tfstate` permet à Terraform d'enregistrer le mapping entre les ressources Terraform et les "real world resources". Par exemple : associer une ressource `aws_instance` à l'ARN (ID) d'une instance EC2 sur AWS créée précédemment.

Le state permet à Terraform de retrouver les ressources créées lors d'une commande `terraform apply` précédente.

**Terraform ne peut pas fonctionner correctement sans state** [:book:](https://www.terraform.io/language/state/purpose) : si le state est perdu, Terraform ne se souvient plus des ressources créées précédemment et les recrées (duplication des ressources).

---

### State [:book:](https://www.terraform.io/language/state)

Le state contient les propriétés de toutes les ressources créées dans la configuration. Ainsi, si la configuration crée une base de données avec un nom d'utilisateur et un mot de passe, alors le state contiendra ces credentials (même si on utilise des variables).

**Il est donc recommandé de ne pas commmiter le contenu de ce state dans un VCS**.

Le fichier `terraform.tfstate.backup` contient le state tel qu'il était **à l'itération précédente**.

---


### State [:book:](https://www.terraform.io/language/state)

Le state permet aussi d'enregistrer des metadata. Par exemple, les dépendances entre les ressources sont enregistrées pour permettre de les détruire dans le bon ordre après avoir effacé une ressource de la configuration.

Par défaut, le state est raffraîchi automatiquement par les commandes `terraform plan` et `terraform apply` pour prendre en compte les changememnts qui ont pu avoir lieu en dehors de Terraform. Il est possible de désactiver le raffraichissement du state pour gagner en performance pour les grosses infras.

---

### Git : fichier .gitignore

Fichier `.gitignore` de référence pour Terraform : [:link:](https://github.com/github/gitignore/blob/main/Terraform.gitignore)

- :file_folder: `.terraform`
- :page_facing_up: `terraform.tfstate`
- :page_facing_up: `terraform.tfstate.backup`

A commiter :
- Le fichier `terraform.lock.hcl` doit être commité
- Les fichier `*.tfvars` peuvent être commités s'ils ne contiennent pas de valeurs sensibles.

---

### Version constraints & lock file

---

### Exercices 2 : Write Terraform Configuration

Vous avez 3 heures pour avancer le plus possible dans l'exercice suivant :

- [Configuration Language](https://learn.hashicorp.com/collections/terraform/configuration-language)
    - **Objectif :** apprendre à écrire une config Terraform from scratch
    
Pour ceux qui ont terminé en avance :
- Survoler les différentes pages de la deuxième section de la doc officielle : [Introduction to Terraform](https://www.terraform.io/intro)
    - Faire le lien avec ce que vous avez déjà appris

---


| Au programme pour la suite du chapitre |
|-|
