app_name = "theapp"

pod_types = {
  nginx = {
    number = 2
    image  = "nginx:1.22.0-perl"
    port   = 80
  }
  jenkins = {
    number = 1
    image  = "jenkins:2.60.3"
    port   = 8080
  }
}
