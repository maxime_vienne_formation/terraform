variable "app_name" {
  description = "The name of my application"
  type        = string
  default     = "myapp"
}

variable "pod_types" {
  type = map(object({
    number = number
    image  = string
    port   = number
  }))
}
