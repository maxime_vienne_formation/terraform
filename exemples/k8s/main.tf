terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.11.0"
    }
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "minikube"
}

locals {
  pod_instances = flatten([
    for pod_type_name, pod_type in var.pod_types : [
      for i in range(pod_type.number) : merge(pod_type, {
        idx           = i
        pod_type_name = pod_type_name
      })
    ]
  ])
}

resource "kubernetes_pod" "test" {
  for_each = { for i, pod_type in local.pod_instances : i => pod_type }

  metadata {
    name = "${each.value.pod_type_name}-${each.value.idx}"
    labels = {
      pod_type_name = each.value.pod_type_name
    }
  }

  spec {
    container {
      image = each.value.image
      name  = each.value.pod_type_name

      port {
        container_port = each.value.port
      }
    }
  }
}

resource "kubernetes_service" "example" {
  for_each = var.pod_types

  metadata {
    name = each.key
  }
  spec {
    selector = {
      pod_type_name = each.key
    }
    port {
      port        = each.value.port
      target_port = each.value.port
    }

    type = "LoadBalancer"
  }
}
