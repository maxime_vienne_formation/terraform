---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![w:400px](./img/terraform_logo.png)


Intoduction à Terraform

---

### Terraform : Qu'est-ce que c'est ? [:book:](https://www.terraform.io/intro)

> HashiCorp Terraform is an infrastructure as code tool that lets you define both cloud and on-prem resources in human-readable configuration files that you can version, reuse, and share. You can then use a consistent workflow to provision and manage all of your infrastructure throughout its lifecycle.

- **Infrastructure as Code (IaaS)** tool
- Human-readable configuration file : **langage déclaratif**
- **Versioning :** avec Git (ou alternative)
- **Consitent workflow :** write, plan, apply

---

### Terraform : Qu'est-ce que c'est ? [:book:](https://www.terraform.io/intro)

> Terraform codifies cloud APIs into declarative configuration files. Terraform can manage low-level components like compute, storage, and networking resources, as well as high-level components like DNS entries and SaaS features

Concrètement, Terraform Open Source est une **CLI** qui permet de créer des ressources **IaaS**, **PaaS**, ou **SaaS** en parlant aux API de ces services.

---

### Infrastructure as Code (IaC) [:book:](https://learn.hashicorp.com/tutorials/terraform/infrastructure-as-code?in=terraform/aws-get-started) [:video_camera:](https://we.tl/t-VfyOqZSfbe)

- Décrire et déployer l'infrastructure à l'aide de **fichiers de configuration déclaratifs**
- Workflow legacy VS scripts VS IaC
- Notions liées :
    - Idempotence
    - Mutable VS immutable
    - Pet VS cattle
- Langage déclaratif : on décrit le résultat attendu, et non pas les étapse pour atteindre ce résultat

---

### Site officiel : [terraform.io](https://www.terraform.io/)

- **Overview**
- **Use cases**
- **Editions** : <u>open source (OSS)</u>, terraform cloud, enterprise [:book:](https://www.terraform.io/intro/terraform-editions)
- **Registry** : <u>providers</u> et modules
- **<u>Tutorials</u>**
- **<u>Docs</u>**
- **Community** : forum, bug tracker, training, <u>certification</u>

---

### Documentation

<table>
<tr>
<td style="vertical-align: top">
Aujourd'hui : <b>Les bases</b>

- [Intoduction to Terraform](https://www.terraform.io/intro)
- [Configuration Language](https://www.terraform.io/language)
- [Terraform CLI](https://www.terraform.io/cli)
- [Terraform Cloud](https://www.terraform.io/cloud-docs)
- [Terraform Tools](https://www.terraform.io/docs/terraform-tools)
</td>

<td style="vertical-align: top">
La semaine prochaine : <b>Aller plus loin</b>

- [Plugin Development](https://www.terraform.io/plugin)
- [Registry Publishing](https://www.terraform.io/registry)
- [CDK for Terraform](https://www.terraform.io/cdktf)
- [Plugin Development](https://www.terraform.io/plugin)
- [Certification : explication](https://www.hashicorp.com/certification/terraform-associate)
</td>
</tr>
</table>

- [Glossary](https://www.terraform.io/docs/glossary) : pour les définitions des (nombreux) mots clés

---

### Registry : [providers](https://registry.terraform.io/browse/providers)

![h:300px](./img/registry_providers.png) ![w:550px](https://mktg-content-api-hashicorp.vercel.app/api/assets?product=terraform&version=v1.2.2&asset=website%2Fimg%2Fdocs%2Fintro-terraform-apis.png&width=2048&height=644)

Les providers sont des **plugins** installables qui permettent à Terraform d'interagir avec les **API** d'un service **IaaS**, **PaaS**, ou **SaaS**. <u>Terraform ne sert à rien sans provider.</u>

---

### Registry : [modules](https://registry.terraform.io/browse/modules)

![h:300px](./img/registry_modules.png)

Les modules sont des **configs Terraform réutilisables**. Vous pouvez créer vos propres modules ou utiliser ceux de la registry.

**Exemple :** [module AWS security-group](https://registry.terraform.io/modules/terraform-aws-modules/security-group/aws/latest)

---

### Workflow : write, plan, apply [:book:](https://www.terraform.io/intro/core-workflow)

![h:500px](https://mktg-content-api-hashicorp.vercel.app/api/assets?product=terraform&version=v1.2.2&asset=website%2Fimg%2Fdocs%2Fintro-terraform-workflow.png&width=2048&height=1798)

---

### Exercices 1 : Get Started

Vous avez une heure pour faire l'exercice suivant :

- [Get Started - AWS](https://learn.hashicorp.com/collections/terraform/aws-get-started)
    - Région : Paris (`eu-west-3`)
        - AMI 1 (Amazon Linux) : `ami-021d41cbdefc0c994`
        - AMI 2 (Ubuntu) : `ami-0042da0ea9ad6dd83`
    - <u>Ne pas faire le dernier exercie "Store Remote State"</u>

**Pour ceux qui ont terminé en avance :** page suivante :arrow_down:

---

### Exercices 1 : Get Started

Pour ceux qui ont terminé en avance :

- <u>Lire uniquement</u> : Get Started - [Docker](https://learn.hashicorp.com/collections/terraform/docker-get-started), [Azure](https://learn.hashicorp.com/collections/terraform/azure-get-started), [GCP](https://learn.hashicorp.com/collections/terraform/gcp-get-started)
    - **Objectif :** voir ce qui change par rapport à l'exercice sur AWS
- Survoler les différentes pages de la première section de la doc officielle : [Introduction to Terraform](https://www.terraform.io/intro)
    - Faire le lien avec ce que vous avez déjà appris

---

### Avantages de Terraform

---

### Terraform VS Alternatives

---

### Quizz

[Insérer lien du quizz]
